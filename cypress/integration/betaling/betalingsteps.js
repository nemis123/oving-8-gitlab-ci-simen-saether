import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});


When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').type('Einar Testens');
    cy.get('#address').type('Testens gate 17');
    cy.get('#postCode').type('8913');
    cy.get('#city').type('Testers by');
    cy.get('#creditCardNo').type('0000000000000000');
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('form').submit();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.contains('Din ordre er registrert').should('exist');
});

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('6');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').clear();
    cy.get('#address').clear();
    cy.get('#postCode').clear();
    cy.get('#city').clear();
    cy.get('#creditCardNo').clear().type('g').blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    
    //cy.contains('Feltet må ha en verdi').should('exist');
    //cy.contains('Kredittkortnummeret må bestå av 16 siffer').should('exist');

    cy.get('.formError').should('have.length', 5);

});